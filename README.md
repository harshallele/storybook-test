# storybook-test

## Project setup
```
npm install
```

### Compile and start storybook component explorer (on port 6006)
```
npm run storybook
```

### Compile and serve app (on port 8080) (Optional)
```
npm run serve
```

