import PieChart from "../src/components/PieChart.vue";
import { action } from '@storybook/addon-actions';

export default {
    title: 'PieChart',
    component: PieChart
};

export const Default = () => ({
    components: {PieChart},
    template:`
    <div class="container">
        <div class="col-6">
            <PieChart @click="click" @hover="hover"/>
        </div>
    </div>
    `,
    methods: {
        click: action('plotly_click'),
        hover: action('plotly_hover'),
    }
})

