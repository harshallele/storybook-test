import Form from "../src/components/Form.vue";
import { action } from '@storybook/addon-actions';

export default {
    title: 'Form',
    component: Form,
};

export const Default = () => ({
    components:{Form},
    template: `
        <div class="container">
            <Form @submit="formSubmit"/>
        </div>
    `,
    methods:{
        formSubmit: action('formsubmit')
    }
});