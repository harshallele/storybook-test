import { action } from '@storybook/addon-actions';
import TodoItem from "../src/components/TodoItem.vue";

export default {
    title: 'TodoItem',
    component: TodoItem,
  };

export const Default = () => ({
  components: {TodoItem},
  template: `
    <div class="container">
      <TodoItem text="This is a normal item" v-on:todo-change="action"/>
    </div>
  `,
  methods:{
    action: action('checkbox')
  }
})

export const disabled = () => ({
  components: {TodoItem},
  template: `
    <div class="container">
      <TodoItem text="This is a disabled item" v-on:todo-change="action" :disabled="true"/>
    </div>
  `,
})