
import TodoList from "../src/components/TodoList.vue";
import { action } from '@storybook/addon-actions';
export default {
    title: 'TodoList',
    component: TodoList,
};

export const Default = () => ({
    components:{TodoList},
    template:`
        <div class="container">
            <TodoList :items="tasks" @checkbox-changed="logInput"/>
        </div>
    `,
    computed:{
        tasks: () => [
            {title: "this is a test 1"},
            {title: "this is a test 2"},
            {title: "this is a test 3"},
        ]
    },
    methods: {
        logInput: action('logInput')
    }
});
